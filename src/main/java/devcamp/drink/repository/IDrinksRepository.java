package devcamp.drink.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import devcamp.drink.model.CDrinks;

public interface IDrinksRepository extends JpaRepository<CDrinks , Long> {
    
}
